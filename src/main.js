import Vue from 'vue'
import App from './App.vue'
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.min.js';

//import Icon from 'vue-awesome/src/components/Icon.vue';
//Vue.component('icon', Icon);

Vue.config.productionTip = false


new Vue({
  render: h => h(App),
}).$mount('#app')
